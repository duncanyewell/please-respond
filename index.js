var https = require('https');

const DEFAULT_TIME = 60000;
const args = process.argv.slice(2);

const dataGatherDuration = args[0] || DEFAULT_TIME;
const data = [];

const options = {
    host: "stream.meetup.com",
    path: "/2/rsvps",
}

console.log("\nGathering Data... \n");

const request = https.get(options, function(response) {
    response.on("data", onReceiveEventData);
})

function onReceiveEventData(eventData) {
    data.push(JSON.parse(eventData));
}

function onDataComplete() {
    request.destroy();

    const countryData = [];

    data.forEach((rsvp) => {
        const countryObj = findObjectByCountry(countryData, rsvp.group.group_country);
        
        if(!countryObj) {
            countryData.push({name: rsvp.group.group_country, totalGuests: rsvp.guests});
        } else { 
            countryObj.totalGuests += rsvp.guests;
        }
    });

    countryData.sort((prev, current) => (prev.totalGuests > current.totalGuests) ? -1 : 1);

    const maxFutureEvent = data.reduce((prev, current) => {
        return (((prev.event.time > current.event.time) ? prev : current))
    });

    const furthestDate = new Date(maxFutureEvent.event.time);

    // console.log("\nTotal # of RSVPs: " + data.length.toString());
    // console.log("Future Date: " + furthestDate);
    // console.log("Future Event URL: " + maxFutureEvent.event.event_url.toString());
    // console.log("TOP 3 Countrys for RSVPs: #1 " + countryData[0].name + " with " + countryData[0].totalGuests + " RSVPs, #2 " + countryData[1].name + " with " + countryData[1].totalGuests + " RSVPs, #3 " + countryData[2].name + " with " + countryData[2].totalGuests + " RSVPs" );
    
    console.log(data.length.toString() + "," 
        + furthestDate + "," 
        + maxFutureEvent.event.event_url.toString() + "," 
        + countryData[0].name + "," 
        + countryData[0].totalGuests + "," 
        + countryData[1].name + "," 
        + countryData[1].totalGuests + "," 
        + countryData[2].name + "," 
        + countryData[2].totalGuests);
}

function findObjectByCountry(countrys, value) {
    for (let i = 0; i < countrys.length; i++) {
        if(countrys[i].name === value) {
            return countrys[i];
        }
    }
    return null;
}

setTimeout(onDataComplete, dataGatherDuration);